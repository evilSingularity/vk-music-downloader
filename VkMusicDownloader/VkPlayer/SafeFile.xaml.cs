﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VkPlayer.Extensions;

namespace VkPlayer
{
    /// <summary>
    /// Логика взаимодействия для SafeFile.xaml
    /// </summary>
    public partial class SafeFile
    {
        public SafeFile()
        {
            InitializeComponent();
            TextBox.IsEnabled = false;
            DownloadButton.IsEnabled = false;
            ProgressBar.Minimum = 0;
            ProgressBar.Maximum = 100;
        }
        public static string Folder;
        /// <summary>
        /// Обработчик события выбора директории
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DirectoryButton_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new FolderBrowserDialog();
           var result=dlg.ShowDialog(this.GetIWin32Window());
           if (result.ToString() == "OK")
           {
               Folder = dlg.SelectedPath;
               TextBox.Text = Folder;
           }
            DownloadButton.IsEnabled = true;
        }

        /// <summary>
        /// Обработчик события Скачать
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DownloadButton_Click(object sender, RoutedEventArgs e)
        {
            var client = new WebClient();
            client.DownloadProgressChanged += DownloadProgressChanged;
            client.DownloadFileCompleted += DownloadFileCompleted;
            client.DownloadFileAsync(new Uri(Player.Mp3Url), String.Format("{0}\\{1}.mp3", Folder, Player.FileName));
          
        }
        private void DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            Close();
        }

        private void DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            ProgressBar.Value = e.ProgressPercentage;
            ProgressLabel.Content = String.Format("{0}%",ProgressBar.Value);
        }
       
    }
}
