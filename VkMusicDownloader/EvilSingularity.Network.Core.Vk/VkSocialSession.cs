﻿using System.IO;
using System.Net;
using EvilSingularity.Network.Core.Interfaces;
using HtmlAgilityPack;

namespace EvilSingularity.Network.Core.Vk

{
    public class VkSocialSession : ISocialSession
    {
        public string UserName { get; set; }

    public bool Auth(string login, string pass)
    {

        ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
        var reqGet = WebRequest.Create("http://m.vk.com/");
        try
        {
            var sr = new StreamReader(reqGet.GetResponse().GetResponseStream());
            var s = sr.ReadToEnd();

//Парсим результат и вытаскиваем action url

            var doc1 = new HtmlDocument();
            doc1.LoadHtml(s);
            var bodyNode = doc1.DocumentNode.SelectSingleNode("//div[@class='form_item fi_fat']/form");
            var actionUrl = bodyNode.Attributes["action"].Value;


//POST запрос

            Cookies = new CookieContainer();
            ServicePointManager.Expect100Continue = false;
            var request = (HttpWebRequest) WebRequest.Create(actionUrl);
            request.CookieContainer = Cookies;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            using (var requestStream = request.GetRequestStream())
            using (var writer = new StreamWriter(requestStream))
            {
                writer.Write("email={0}&pass={1}", login, pass);
            }
            using (var responseStream = request.GetResponse().GetResponseStream())
            using (var reader = new StreamReader(responseStream))
            {
                var result = reader.ReadToEnd();

//Парсим результат запроса
                var doc2 = new HtmlDocument();
                doc2.LoadHtml(result);
                try

//Ищем UserName в соответствующем ноде, возвращаем 1, в противном случае 0 

                {
                    var bodyNode2 = doc2.DocumentNode.SelectSingleNode("//div[@class='ip_user_link']/a");
                    UserName = bodyNode2.Attributes["data-name"].Value;
                    return true;

                }
                catch
                {
                    return false;
                }
            }
        }
        catch

//WebExсeption и т.д.
        {
            return false;
        }
    }

        public CookieContainer Cookies { get; private set; }
    }

}




