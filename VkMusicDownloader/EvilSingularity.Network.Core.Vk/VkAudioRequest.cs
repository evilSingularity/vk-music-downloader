﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using EvilSingularity.Network.Core.Extensions;
using EvilSingularity.Network.Core.Interfaces;
using HtmlAgilityPack;

namespace EvilSingularity.Network.Core.Vk
{
    public class VkAudioRequest : IAudioRequest

    {
            public List<AudioInfo> Records { get; set; }
            public void AudioList(string searchString, CookieContainer cookies, string sessionUrl)
            {
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };



                //POST запрос
                var requestAudio = (HttpWebRequest)WebRequest.Create("http://m.vk.com/audio?act=search");
                requestAudio.CookieContainer = cookies;
                requestAudio.Method = "POST";
                requestAudio.ContentType = "application/x-www-form-urlencoded";
                using (var requestStream = requestAudio.GetRequestStream())
                using (var writer = new StreamWriter(requestStream))
                {
                    writer.Write("&q={0}", searchString);
                }
                using (var responseStream = requestAudio.GetResponse().GetResponseStream())
                using (var reader = new StreamReader(responseStream))
                {
                    var resultAudio = reader.ReadToEnd();



                    //Парсим результат запроса

                    var doc = new HtmlDocument();
                   doc.LoadHtml(resultAudio);

                    try

                        //Формирование плейлиста
                    {
                        Records = new List<AudioInfo>();
                        foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//div[@class='ai_body']"))
                        {
                            string url = link.ChildElement("input").Attributes["value"].Value;
                            string duration = link.ChildElement("div", "ai_dur").InnerText;
                            var ailabel = link.ChildElement("div", "ai_label");
                           string artist = HttpUtility.HtmlDecode(ailabel.ChildElement("span", "ai_artist").InnerText);
                           string title = HttpUtility.HtmlDecode(ailabel.ChildElement("span", "ai_title").InnerText);
                            Records.Add(new AudioInfo(artist, title, duration, url));
                        }

                    }
                    catch
                    {

                    }
                }
            }
        }
    }

