﻿using System.Linq;
using HtmlAgilityPack;

namespace EvilSingularity.Network.Core.Extensions
{
    public static class HtmlNodeExtensions
    {
        public static HtmlNode ChildElement(this HtmlNode n, string name, string cls)
        {
            return AllChildElements(n, name, cls).FirstOrDefault();
        }

        public static HtmlNode ChildElement(this HtmlNode n, string name)
        {
            return AllChildElements(n, name).FirstOrDefault();
        }

        public static IQueryable<HtmlNode> AllChildElements(this HtmlNode n, string name, string cls = null)
        {
            var q = n.ChildNodes.AsQueryable().Where(c => c.Name == name);
            if (!string.IsNullOrWhiteSpace(cls))
            {
                q = q.Where(c => c.Attributes["class"].Value == cls);
            }

            return q;
        }

    }
}

