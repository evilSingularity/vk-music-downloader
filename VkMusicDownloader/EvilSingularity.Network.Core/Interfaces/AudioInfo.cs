﻿namespace EvilSingularity.Network.Core.Interfaces
{
    public class AudioInfo
    {

        /// <summary>
       /// Имя исполнителя
       /// </summary>
        public string Artist { get; set; }
        /// <summary>
        /// Название композиции
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Длительность трека
        /// </summary>
        public string Duration { get; set; }
        /// <summary>
        /// Ссылка для воспроизведения
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Конструктор класса
        /// </summary>
        public AudioInfo(string artist, string title, string duration, string url)
        {
            Artist = artist;
            Title = title;
            Duration = duration;
            Url = url;

        }

    }
}
