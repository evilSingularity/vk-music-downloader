﻿using System.Collections.Generic;
using System.Net;

namespace EvilSingularity.Network.Core.Interfaces
{
 
    public interface IAudioRequest

    {
        List<AudioInfo> Records { get; set; }
        void AudioList(string searchString, CookieContainer cookies, string sessionUrl);
    }
}
