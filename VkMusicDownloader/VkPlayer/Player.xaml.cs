﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Windows.Threading;
using EvilSingularity.Network.Core.Interfaces;
using EvilSingularity.Network.Core.Vk;



namespace VkPlayer
{
    /// <summary>
    /// Логика взаимодействия для Player.xaml
    /// </summary>
    public partial class Player
    {
        public Player()
        {
            InitializeComponent();
            IsPlaying(false);
            BtnDownload.IsEnabled = false;
        }
     /// <summary>
     /// Флаг активности кнопок плеера
     /// </summary>
    
	private void IsPlaying(bool bValue)
	{
	BtnStop.IsEnabled = bValue;
    BtnMoveBackward.IsEnabled = bValue;
	BtnMoveForward.IsEnabled = bValue;
    BtnPlay.IsEnabled = bValue;
	}
        
        public static string Mp3Url;
        public static string FileName;
        public List<AudioInfo> Audiolist;

        /// <summary>
        /// Обработчик события Play
        /// Изменение текста кнопки в зависимости от текущего состояния
        /// </summary>

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            if (BtnPlay.Content.ToString() == "Open")
                Open();
            else
            {
                IsPlaying(true);
                if (BtnPlay.Content.ToString() == "Play")
                {
                    MediaEl.Play();
                    BtnPlay.Content = "Pause";
                }
                else
                {
                    MediaEl.Pause();
                    BtnPlay.Content = "Play";
                }
            }

        }
    
      void timer_Tick(object sender, EventArgs e)
      {

          if (MediaEl.Source != null)
          {
              if (MediaEl.NaturalDuration.HasTimeSpan)
              {
                  MediaPosition.Content = String.Format("{0}", MediaEl.Position.ToString(@"mm\:ss"));
                  MediaDuration.Content = String.Format("{0}", MediaEl.NaturalDuration.TimeSpan.ToString(@"mm\:ss"));
              }
             
          }
          else
              {
              MediaPosition.Content = "Файл не выбран";
              MediaDuration.Content = "";
              }
      }
  /// <summary>
  /// Обработчик события Stop
  /// Остановка воспроизведения, включение активности кнопки Play
  /// </summary>
 
	private void btnStop_Click(object sender, RoutedEventArgs e)
	{
	    MediaEl.Stop();
	    BtnPlay.Content = "Play";
	    IsPlaying(false);
	    BtnPlay.IsEnabled = true;
	}
	
        /// <summary>
        /// Обработчик события Back
        /// Изменение позиции воспроизведения на -10 секунд
        /// </summary>
      
        private void btnMoveBackward_Click(object sender, RoutedEventArgs e)
        {
            MediaEl.Position = MediaEl.Position - TimeSpan.FromSeconds(10);
        }
 
        /// <summary>
        /// Обработчик события Forward
        /// Изменение позиции воспроизведения на +10 секунд
        /// </summary>
        
        private void btnMoveForward_Click(object sender, RoutedEventArgs e)
        {
            MediaEl.Position = MediaEl.Position + TimeSpan.FromSeconds(10);
        }

        /// <summary>
        /// Обработчик события Find
        /// Вывод результатов поиска
        /// </summary>
       
        private void Find_Click(object sender, RoutedEventArgs e)
        {
            Playlist.UnselectAll();
            Playlist.Items.Clear();
           if (Search.Text != "")
            {
                var newAudioRequest = new VkAudioRequest();
                newAudioRequest.AudioList(Search.Text, MainWindow.SessionCookie, MainWindow.SessionUrl);
                Audiolist = newAudioRequest.Records;
                foreach (var audio in Audiolist)
                {
                    Playlist.Items.Add(String.Format("{0} - {1} [{2}]", audio.Artist, audio.Title,audio.Duration));
                }

            }

            }

        private void Playlist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (Playlist.SelectedItem != null)
            {
            BtnDownload.IsEnabled = true;
            BtnPlay.IsEnabled = true;
            int i = Playlist.SelectedIndex;
               
                if (Audiolist[i].Url.StartsWith("https"))
            Mp3Url=String.Format("http{0}", Audiolist[i].Url.Substring(5));
                else Mp3Url=Audiolist[i].Url;
                FileName = (String.Format("{0} - {1}", Audiolist[i].Artist, Audiolist[i].Title));
            }
    }
        /// <summary>
        /// Обработчик события Download
        /// </summary>
  
        private void BtnDownload_Click(object sender, RoutedEventArgs e)
        {
            var s = new SafeFile();
            s.Show();
            


            }

        private void Playlist_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Open();
        }

        private void Open()
        {
            MediaEl.Source = new Uri(Mp3Url, UriKind.Absolute);
            if (MediaEl.Source != null)
            {
                MediaEl.Play();
                BtnPlay.Content = "Pause";
                IsPlaying(true);
                TitleTextBlock.Text = FileName;
            }
            var timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }
        }

        }
        



        

   

       
        
 
        
 

    


    

