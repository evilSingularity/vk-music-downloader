﻿using System.Net;

namespace EvilSingularity.Network.Core.Interfaces
{
    /// <summary>
    /// Общий интерфейс для всех социальных сетей
    /// </summary>
   public interface ISocialSession
    {
       /// <summary>
       /// Имя пользователя
       /// </summary>
       /// 
       string UserName { get; set; }


       /// <summary>
       /// Авторизация
       /// </summary>
       /// <param name="login">Логин</param>
       /// <param name="pass">Пароль</param>
       /// <returns>1 или 0: подключено/не подключено</returns>
       /// 
       bool Auth(string login, string pass);
       CookieContainer Cookies { get; }

    }
}
