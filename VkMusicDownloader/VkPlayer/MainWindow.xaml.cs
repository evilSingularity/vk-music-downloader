﻿using System;
using System.Net;
using System.Windows;
using EvilSingularity.Network.Core.Interfaces;
using EvilSingularity.Network.Core.Vk;

namespace VkPlayer
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        public static CookieContainer SessionCookie;
        public static string SessionUrl;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
           
            var login = Login.Text;
            var pass = Pass.Password;

            ISocialSession session = new VkSocialSession();
     
            var avt = session.Auth(login, pass);
            SessionCookie = session.Cookies;
     
          if(avt)
            {
                Hide();
                var p = new Player();
                p.Show();
                Close();
                p.TextBlock.Text = String.Format("Добро пожаловать, {0}!", session.UserName);
               
            
              
            }
            else TextBlock.Text = "Ошибка авторизации!";
        }

        }
    }

